#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup


VERSION = '1.0.0'


setup(

    name='pydummymod',
    version = VERSION,
    description = 'pydummymod - Template project for building a Python module',
    long_description = 'pydummymod is a template project for building a Python module, including testing, documentation and packaging',
    classifiers = [
        # How mature is this project? Common values are
        # 3 - Alpha
        # 4 - Beta
        # 5 - Production/Stable
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 2.7',
        'Topic :: Multimedia :: Sound/Audio :: Analysis',
    ],
    url = 'http://www.mufin.com',
    author = 'Lars Fabig',
    author_email = 'lfabig@mufin.com',
    maintainer = 'Lars Fabig',
    maintainer_email = 'lfabig@mufin.com',
    license = 'Other/Proprietary License',
    packages = [
        'dummymod',
        'dummymod.cli'
    ],
    entry_points = {
        'console_scripts' : [
            'dummymod_helloworld = dummymod.cli.dummymod_helloworld:main',
        ]
    },
    install_requires = [
        'pyyaml',
    ],
    include_package_data = True,
    zip_safe=False

)
