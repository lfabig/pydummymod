dummymod.cli package
====================


PyDummyMod command line utilities
-----------------------------------

PyDummyMod command line utilities are installed in the path::

      /usr/local/bin/


dummymod_helloworld
~~~~~~~~~~~~~~~~~~~

.. argparse::
   :ref: dummymod.cli.dummymod_helloworld.parser
   :prog: dummymod_helloworld
