dummymod package
================



Submodules
----------

dummymod.dummy module
---------------------

.. automodule:: dummymod.dummy
    :members:
    :undoc-members:
    :show-inheritance:

dummymod.exceptions module
----------------------------

.. automodule:: dummymod.exceptions
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    dummymod.cli

Module contents
---------------

.. automodule:: dummymod
    :members:
    :undoc-members:
    :show-inheritance:
