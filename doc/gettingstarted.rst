Getting Started
===============

Introduction
------------

PyDummyMod is a an example Python module which explains:

* Python module structure
* Python command line applications
* Exception handling
* Unit testing
* Sphinx based documentation using reStructuredText
* Python package creation using setup.py
* Gitlab CI
* Gitlab Pages documentation deployment

The package includes:

* pydummymod module
* Command line utility
* Sphinx HTML documentation
* Unit tests

The module and scripts can be used cross-platform, e.g. on Linux and Windows.


Prerequisites
-------------

:Note: Documentation below only makes sense if the module is available in an remotely accessible repository.

Check if Python 2.7 is installed and the default version::

        python -V

Make sure pip the `PyPA`_ package manager is installed checking with::

        pip -V

.. _PyPA: https://pip.pypa.io/en/stable/

If pip is not installed follow the instructions `here`_.

.. _here: https://pip.pypa.io/en/stable/installing/.



Installation on Linux
---------------------

Install pydummymod from its private repository::

        sudo -H pip install pydummymod

Alternatively install as root::

        su

        pip install pydummymod

The module itself is installed and several command line utilities::

        pydummymod_helloworld

in the path::

        /usr/local/bin/


Installation on Windows
-----------------------

Install pydummymod_helloworld from its private repository::

        pip install pydummymod_helloworld

The module itself is installed and several command line utilities::

        pydummymod_helloworld.exe

Demo
----

N/A


Uninstall
---------

Uninstall using sudo::

        sudo -H pip uninstall pydummymod

Alternatively uninstall as root::

        su

        pip uninstall pydummymod


