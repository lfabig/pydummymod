.. audioidmon documentation master file, created by
   sphinx-quickstart on Sun Aug 14 22:40:13 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PyDummyMod - an example Python module
=====================================

.. toctree::
   :maxdepth: 4
   :includehidden:

   dummymod

.. include:: gettingstarted.rst

.. include:: ../CHANGES.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


