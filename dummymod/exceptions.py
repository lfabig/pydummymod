 #!/usr/bin/python
# -*- coding: utf-8 -*-

"""This module implements exception classes for a dummy Python module"""

__author__    = "Lars Fabig"
__date__      = "19.01.2018"
__email__     = "lfabig@mufin.com"
__docformat__ = 'reStructuredText'


# Standard library imports ...
import json


class DummyModException(Exception):
    """Base Exception Class"""


class DummyModUnexpectedError(DummyModException):
    """An unexpected error occurred"""



if __name__ == "__main__":
    import doctest
    doctest.testmod()
