#!/usr/bin/python
# -*- coding: utf-8 -*-

"""dummymod_helloworld: Script returns hello world text"""

__author__  = 'Lars Fabig'
__date__    = '19.01.2018'
__email__   = 'lfabig@mufin.com'
__docformat__ = 'reStructuredText'


# Standard library imports ...
import argparse
import json
import pkg_resources
import sys

# Local imports ...
import dummymod



# Init command line parser
parser = argparse.ArgumentParser(prog='dummymod_helloworld', description='Returns hello world text')
parser.add_argument('-v', '--verbose', help='increase output verbosity', action='store_true')
parser.add_argument('--version', help='print version number', action='version', version='%(prog)s {}'.format(pkg_resources.get_distribution('pyaudioidmon').version))


def main():
    # parsing command line options
    args = parser.parse_args()
    # print input parameter
    if args.verbose:
        print "Print Hello World text in JSON format:"
    try:
        # instantiate audioid monitor client
        if args.verbose:
            print "  Init object of class DummyMod ..."
        dummyObj = dummymod.DummyClass()
        # get fingerprint ids
        if args.verbose:
            print "  Get text ...\n"
        response = dummyObj.get_hello_world_text()
        print json.dumps(response, sort_keys=True, indent=2)
    except Exception as err:
        sys.stderr.write("[Error] {} : {}\n".format(type(err).__name__, str(err)))
        sys.exit(1)
    if args.verbose:
        print "Done."
    sys.exit(0)


if __name__ == '__main__':
    main()
