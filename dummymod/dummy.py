#!/usr/bin/python
# -*- coding: utf-8 -*-

"""This module implements HTTP client for the REST API of audioid monitor generation 3 service"""

__author__    = "Lars Fabig"
__date__      = "19.01.2018"
__email__     = "lfabig@mufin.com"
__docformat__ = 'reStructuredText'


# Standard library imports ...
import pkg_resources

# Third-party imports ...


# Local imports ...
import exceptions



class DummyClass:
    """This class implement a dummy class for a Python module.
    """

    def __init__(self):
        """Init class"""
        pass


    def get_client_version(self):
        """Return version of package"""
        return pkg_resources.get_distribution('pydummymod').version


    def get_hello_world_text(self):
        return "Hello World!"


if __name__ == "__main__":
    import doctest
    doctest.testmod()
