#!/usr/bin/python
# -*- coding: utf-8 -*-

# Standard library imports ...
import unittest

# Third-party imports ...
import pkg_resources
import yaml


# Local imports ...
import dummymod



class DummyModTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        """Setup test case"""
        self.data = yaml.load(open("test_config.yml"))["test_data"]
        self.dummyObj = dummymod.DummyClass()


    @classmethod
    def tearDownClass(self):
        """Tear down test case"""
        pass


    # Test get_base_url
    def test_get_client_version(self):
        """Test get_client_version"""
        self.assertEquals(self.dummyObj.get_client_version(), pkg_resources.get_distribution('pydummymod').version)

    # Test get_hello_world_json_text
    def test_get_hello_world_json_text(self):
        """Test get_hello_world_json_text"""
        self.assertEquals(self.dummyObj.get_hello_world_text(), "Hello World!")


if __name__ == "__main__":
    unittest.main()
