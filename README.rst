PyDummyMod
==========

Description
-----------

PyDummyMod is a an example Python module which explains:

* Python module structure
* Python command line applications
* Exception handling
* Unit testing
* Sphinx based documentation using reStructuredText
* Python package creation using setup.py
* Gitlab CI
* Gitlab Pages documentation deployment

The package includes:

* pydummymod module
* Command line utility
* Sphinx HTML documentation
* Unit tests


Prerequisites
-------------

Check if Python 2.7 is the default version::

        python -V

Make sure pip the `PyPA`_ package manager is installed checking with::

        which pip

.. _PyPA: https://pip.pypa.io/en/stable/

If pip is not installed follow the instructions `here`_.

.. _here: https://pip.pypa.io/en/stable/installing/.

Install required packages for development::

        sudo -H pip install -r requirements.txt

Alternatively install as root::

        su

        pip install -r requirements.txt


This will install the following packages required for development:

* `Sphinx`_ and sphinx-argparse extension for generation of documentation.
* `sphinx-argparse`_ a sphinx extension to automatically document CLI programs.

.. _Sphinx: http://www.sphinx-doc.org/en/stable/
.. _sphinx-argparse: http://sphinx-argparse.readthedocs.io
.. _httpretty: https://github.com/gabrielfalcao/httpretty

And the dependencies of the module package pyaudioidmon:

* `requests`_ a HTTP for humans.
* `PyYaml`_ a YAML parser for Python.

.. _Requests: http://docs.python-requests.org
.. _PyYaml: http://pyyaml.org/



Build package and HTML documentation
------------------------------------

:Note: This step is required before testing or running the scripts!

The reason is that the command line utlities, the test and the document generator require the package information to be created first, to retrieve the version number from `setup.py`.

To init the pyaudioidmon package execute::

        ./package_build

which does the following:

* Creates package info
* Create HTML documentation

The HTML documentation is created in the folder::

        ./build/sphinx/html

The Python modules are documented using `reStructuredText`_.

.. _reStructuredText: http://docutils.sourceforge.net/rst.html

To view the HTML documentation in your default browser run the shell script::

        ./show_html_doc



Tests
-----

The Python package includes also unit and integration tests in the subfolder dummymod/tests.

Run unit and mock tests executing in the root directory of the dummymod package::

        ./run_tests


Install
-------

Install executing in the root directory of the dummymod package::

        sudo -H pip install -e .

Alternatively install as root::

        su

        pip install -e .

The module itself is installed and several command line utilities::

        dummymod_helloworld

in the path::

        /usr/local/bin/


Demo
----

N/A



Build distributables
--------------------

To build source distributable run::

        ./package_dist

The package file is stored in::

        ./dist/



Build Debian package
--------------------

Make sure that the following packages are installed::

        sudo apt-get install debhelper dh-virtualenv

To build the Debian package run::

        make-deb

        dpkg-buildpackage -us -uc

The package files are stored here::

        ../



Uninstall
---------

Uninstall using sudo::

        sudo -H pip uninstall pydummymod

Alternatively uninstall as root::

        su

        pip uninstall pydummymod



Cleanup package
---------------

Remove all generated files::

        ./package_clean



Development setup
-----------------

If you want to develop with the installed package you may install the package with::

        sudo -H python setup.py develop

This links the scripts and modules installed system wide to your local folder or the package. The advantage is that code changes will take effect immediately.

To uninstall execute::

        sudo -H python setup.py develop --uninstall

:Note: Some script files have to be uninstalled manually.


Known Issues
------------

N/A


Notes
-----

* The interactive shell `IPython`_ is a great tool for efficient development in `Python`_.
* As reStructuredText viewer you may use `retext`_.

.. _retext: https://github.com/retext-project/retext
.. _Python: https://www.python.org/
.. _IPython: https://ipython.org/

